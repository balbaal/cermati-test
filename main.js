var isAlreadyPopUp = true;
var resStatus = false;
var currentTime;
var getTimeSaved;

function checkingExpired() {
	currentTime = new Date()
	getTimeSaved = JSON.parse(localStorage.getItem("disablePanel"))
	if(getTimeSaved) {
		currentTime.getTime() > getTimeSaved.expired ? localStorage.clear() : null
	}
}

$(window).on("load", function() {
	checkingExpired()
});

$(window).mousemove(function() {
	checkingExpired()
})

// notification action
$("#notif-close").click(function() {
  $("#notif-close").slideUp("slow", function() {});
});

// Sliding panel close
$("#sliding-pan__close").click(function() {
	currentTime = new Date();
  $("#sliding-pan").slideToggle("slow", function() {
    // set on localstorage
    localStorage.setItem("disablePanel", JSON.stringify({
      status: true,
      expired: currentTime.getTime() + 10 * 60 * 1000
    }));
  });
});

// Sliding panel scrooll
// before running checking
// local storage, is it there
$(window).scroll(function() {
  resStatus = localStorage.getItem("disablePanel");
  
  if (!resStatus) {
    if (isAlreadyPopUp) {
      if ($(document).scrollTop() >= $(document).height() / 5) {
        $("#sliding-pan").slideDown("slow", function() {});
      } //else $("#sliding-pan").slideUp("slow", function() {});
    }
  } else checkingExpired()
});
